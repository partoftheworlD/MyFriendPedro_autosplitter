# My Friend Pedro (2019) Autosplitter

# Known bugs:

* **None**

# Instructions

[![How to setup autosplitter](https://img.youtube.com/vi/DpknSPdyots/0.jpg)](https://www.youtube.com/watch?v=DpknSPdyots)

# Something didn't split or split at the wrong time.

If that doesn't work, or if a split of yours split at the wrong time, contact me (NutsVoyage#3548).

# Credits 

Big thanks to Ero
